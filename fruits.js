const fruits = {
  apple: {
    calories: 52.1,
    vitamins: ["A", "C"],
    minerals: ["Calcium", "Iron", "Magnesium"]
  },
  banana: {
    calories: 88.7,
    vitamins: ["A", "C", "B6"],
    minerals: ["Calcium", "Iron", "Magnesium"]

  },
  mango: {
    calories: 59.8,
    vitamins: ["A", "C", "B6"],
    minerals: ["Calcium", "Iron", "Magnesium"]
  }
}

const avgCaloricMen = 2500
const avgCaloricSportMen = 4500
const avgCaloricWomen = 2000
const avgCaloricSportWomen = 4000

console.log('men avg: ', avgCaloricMen, 'sporting men avg: ', avgCaloricSportMen)
console.log('women avg: ', avgCaloricWomen, 'sporting women avg: ', avgCaloricSportWomen)

//case 1 -- avg person eating 5 mangos, 10 bananas and 45 apples
let calories = fruits.mango.calories * 5 + fruits.banana.calories * 10 + fruits.apple.calories * 45
console.log('calories case 1: ', calories)
